﻿using System;

namespace week5_ex24_a
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Exercise A------");
            Method();
            Method();
            Method();
            Method();
            Method();
            Console.WriteLine();
            Console.WriteLine("------Exercise B------");
            Console.WriteLine(ExB("Kadin"));
            Console.WriteLine(ExB("Jeff"));
            Console.WriteLine(ExB("James"));
            Console.WriteLine(ExB("Jen"));
            Console.WriteLine(ExB("Jesse"));
            Console.WriteLine();
            Console.WriteLine("------Exercise B------");
            Console.WriteLine("(Converts method to string)");
            Console.WriteLine();
            Console.WriteLine("------Exercise D------");
            ExD(5, 3, 4);
            ExD(4, 7, 7);
            ExD(5, 6, 3);
            ExD(7, 8, 0);
            ExD(9, 1, 2);
            

        }
        static void Method()
        {
            Console.WriteLine("My first method");
        }
        static string ExB(string name)
        {
            return $"Hello {name}";
        }
        static void ExD(int num1, int num2, int num3)
        {
            Console.WriteLine($"{num1*num2*num3}");
        }
         
    }
}
